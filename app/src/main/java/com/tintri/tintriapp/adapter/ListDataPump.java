package com.tintri.tintriapp.adapter;

import com.tintri.tintriapp.model.TintriObject;
import com.tintri.tintriapp.model.VirtualMachine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shosharma on 11/25/17.
 */

public class ListDataPump<T extends TintriObject> {
    private List<T> tintriObjects;
    private Map<String, List<String>> data;

    public ListDataPump(List<T> tintriObjects){
        this.tintriObjects = tintriObjects;
        data = new HashMap<>();
        populateData();
    }

    public Map<String, List<String>> getData(){
        return data;
    }

    private void populateData(){
        for(T tintriObject: tintriObjects){
            String key = tintriObject.getName();
            List<String> value = getProperties(tintriObject);
            data.put(key, value);
        }
    }

    private List<String> getProperties(T tintriObject){
        List<String> properties = new ArrayList<>();
        Method methods[] = tintriObject.getClass().getMethods();
        for (Method method : methods) {
            if (method.getReturnType().equals(String.class) && method.getName().startsWith("get"))
                try {
                    properties.add(method.getName().replaceAll("get", "") + ": " + (String) method.invoke(tintriObject, null));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
        }
        return properties;
    }

    public static void main(String as[]){

    }
}
