package com.tintri.tintriapp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tintri.tintriapp.adapter.CustomExpandableListAdapter;
import com.tintri.tintriapp.adapter.ListDataPump;
import com.tintri.tintriapp.model.ServiceGroup;
import com.tintri.tintriapp.request.TintriRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by shosharma on 12/9/17.
 */

public class ServiceGroupTab extends Fragment {

    private String cookie;
    ExpandableListView serviceGroupListView;
    List<String> expandableListTitle;
    Map<String, List<String>> expandableListDetail;
    CustomExpandableListAdapter expandableListAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View serviceGroupView = inflater.inflate(R.layout.servicegroup_frag, container, false);
        cookie = getActivity().getIntent().getExtras().getString("Cookie");
        TintriUtil.handleSSLHandshake();

        serviceGroupListView = serviceGroupView.findViewById(R.id.serviceGroupListView);
        sendGetRequest();
        return serviceGroupView;
    }

    private void sendGetRequest(){
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        TintriRequest request = new TintriRequest(Request.Method.GET,
                Constants.getServiceGroupsURLString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            List<ServiceGroup> sgs = toModel(response);
                            Log.d(Constants.TAG, sgs.toString());
                            renderModel(sgs);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(Constants.TAG, error+"");
                    }
                });
        request.setCookie(cookie);
        requestQueue.add(request);
    }

    public List<ServiceGroup> toModel(String response) throws JSONException {
        JSONObject object = new JSONObject(response);
        JSONArray items = (JSONArray) object.get("items");
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<ServiceGroup> serviceGroups = new ArrayList<>();
        for(int i=0;i<items.length();i++){
            try {
                serviceGroups.add(objectMapper.readValue(items.get(i).toString(),ServiceGroup.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return serviceGroups;
    }

    public void renderModel(List<ServiceGroup> sgs){
        ListDataPump<ServiceGroup> dataSgs = new ListDataPump<>(sgs);
        expandableListDetail = dataSgs.getData();
        expandableListTitle = new ArrayList<String>(dataSgs.getData().keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getActivity().getApplicationContext(), expandableListTitle, expandableListDetail);
        serviceGroupListView.setAdapter(expandableListAdapter);
    }
}
