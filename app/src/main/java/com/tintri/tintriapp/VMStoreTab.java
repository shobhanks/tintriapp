package com.tintri.tintriapp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tintri.tintriapp.adapter.CustomExpandableListAdapter;
import com.tintri.tintriapp.adapter.ListDataPump;
import com.tintri.tintriapp.model.VMStore;
import com.tintri.tintriapp.request.TintriRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by shosharma on 12/9/17.
 */

public class VMStoreTab extends Fragment {

    private String cookie;
    ExpandableListView vmStoreListView;
    List<String> expandableListTitle;
    Map<String, List<String>> expandableListDetail;
    CustomExpandableListAdapter expandableListAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vmStoreView = inflater.inflate(R.layout.vmstore_frag, container, false);
        cookie = getActivity().getIntent().getExtras().getString("Cookie");
        TintriUtil.handleSSLHandshake();

        vmStoreListView = vmStoreView.findViewById(R.id.vmStoreListView);
        sendGetRequest();
        return vmStoreView;
    }

    private void sendGetRequest(){
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        TintriRequest request = new TintriRequest(Request.Method.GET,
                Constants.getVMStoresURLString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            List<VMStore> vmstores = toModel(response);
                            Log.d(Constants.TAG, vmstores.toString());
                            renderModel(vmstores);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(Constants.TAG, error+"");
                    }
                });
        request.setCookie(cookie);
        requestQueue.add(request);
    }

    public List<VMStore> toModel(String response) throws JSONException {
        JSONArray items = new JSONArray(response);
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<VMStore> vmStores = new ArrayList<>();
        for(int i=0;i<items.length();i++){
            try {
                vmStores.add(objectMapper.readValue(items.get(i).toString(),VMStore.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return vmStores;
    }

    public void renderModel(List<VMStore> vmstores){
        ListDataPump<VMStore> dataVms = new ListDataPump<>(vmstores);
        expandableListDetail = dataVms.getData();
        expandableListTitle = new ArrayList<String>(dataVms.getData().keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getActivity().getApplicationContext(), expandableListTitle, expandableListDetail);
        vmStoreListView.setAdapter(expandableListAdapter);
    }
}
