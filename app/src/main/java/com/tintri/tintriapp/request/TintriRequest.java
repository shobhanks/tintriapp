package com.tintri.tintriapp.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shosharma on 11/24/17.
 */

public class TintriRequest extends StringRequest {
    private static final String TAG = TintriRequest.class.getSimpleName();

    private String cookie;

    public TintriRequest(int method,
                         String url,
                         Response.Listener<String> listener,
                         Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();
        Log.d(TAG, cookie);
        headers.put("Cookie", cookie);
        return headers;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Log.d(TAG, response.data.toString().length()+"");
        return Response.success(new String(response.data)+"", HttpHeaderParser.parseCacheHeaders(response));
    }
}
