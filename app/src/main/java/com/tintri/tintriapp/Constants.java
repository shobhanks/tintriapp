package com.tintri.tintriapp;

import com.android.volley.toolbox.StringRequest;

/**
 * Created by shosharma on 11/25/17.
 */

public class Constants {

    public static String HOST_NAME = "";
    public static final String SCHEME = "https";
    public static final String LOGIN_API = "/api/v310/session/login";
    public static final String VM_API = "/api/v310/vm";
    public static final String VM_STORE_API = "/api/v310/vmstore";
    public static final String SERVICE_GROUP_API = "/api/v310/servicegroup";
    public static final String VM_STORE_POOL_API = "/api/v310/vmstorePool";
    public static final String TAG = "["+"TintriApp"+"]";

    public static String getLoginURLString() {
        return SCHEME + "://" + HOST_NAME + LOGIN_API;
    }

    public static String getVMsURLString() {
        return SCHEME + "://" + HOST_NAME + VM_API;
    }

    public static String getVMStoresURLString() {
        return SCHEME + "://" + HOST_NAME + VM_STORE_API;
    }

    public static String getServiceGroupsURLString() {
        return SCHEME + "://" + HOST_NAME + SERVICE_GROUP_API;
    }

    public static String getVmStorePoolURLString() {
        return SCHEME + "://" + HOST_NAME + VM_STORE_POOL_API;
    }
}
