package com.tintri.tintriapp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tintri.tintriapp.adapter.CustomExpandableListAdapter;
import com.tintri.tintriapp.adapter.ListDataPump;
import com.tintri.tintriapp.model.VirtualMachine;
import com.tintri.tintriapp.request.TintriRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by shosharma on 12/9/17.
 */

public class VMTab extends Fragment {

    private String cookie;
    ExpandableListView vmListView;
    List<String> expandableListTitle;
    Map<String, List<String>> expandableListDetail;
    CustomExpandableListAdapter expandableListAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vmView = inflater.inflate(R.layout.vmtab_frag, container, false);
        cookie = getActivity().getIntent().getExtras().getString("Cookie");
        TintriUtil.handleSSLHandshake();

        vmListView = vmView.findViewById(R.id.vmListView);
        sendGetRequest();
        return vmView;
    }

    private void sendGetRequest(){
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        TintriRequest request = new TintriRequest(Request.Method.GET,
                Constants.getVMsURLString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            List<VirtualMachine> vms = toModel(response);
                            Log.d(Constants.TAG, vms.toString());
                            renderModel(vms);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(Constants.TAG, error+"");
                    }
                });
        request.setCookie(cookie);
        requestQueue.add(request);
    }

    public List<VirtualMachine> toModel(String response) throws JSONException {
        JSONObject object = new JSONObject(response);
        JSONArray items = (JSONArray) object.get("items");
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<VirtualMachine> virtualMachines = new ArrayList<>();
        for(int i=0;i<items.length();i++){
            try {
                virtualMachines.add(objectMapper.readValue(items.get(i).toString(),VirtualMachine.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return virtualMachines;
    }

    public void renderModel(List<VirtualMachine> vms){
        ListDataPump<VirtualMachine> dataVms = new ListDataPump<>(vms);
        expandableListDetail = dataVms.getData();
        expandableListTitle = new ArrayList<String>(dataVms.getData().keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getActivity().getApplicationContext(), expandableListTitle, expandableListDetail);
        vmListView.setAdapter(expandableListAdapter);
    }

}
