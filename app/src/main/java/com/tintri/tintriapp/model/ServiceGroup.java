package com.tintri.tintriapp.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by shosharma on 12/9/17.
 */

public class ServiceGroup implements TintriObject {
    private String name;
    private SyncReplPolicy syncReplPolicy;

    @Override
    public String getName() {
        return name;
    }

    public String getClusterIp() {
        if(isSyncRepl())
            return getSyncReplPolicy().getSyncReplConfig().getClusterIpConfig().getIpAddress();
        return "";
    }

    public String getPrimarySyncRepl() {
        if(isSyncRepl()){
            SyncReplVmstoreInfo syncReplVmstoreInfoList[] = getSyncReplPolicy().getSyncReplConfig().getSyncReplVmstores();
            for(SyncReplVmstoreInfo syncReplVmstoreInfo: syncReplVmstoreInfoList){
                if(syncReplVmstoreInfo.isCreateAsPrimary())
                    return syncReplVmstoreInfo.getHost();
            }
        }
        return "";
    }

    public String getSecondarySyncRepl() {
        if(isSyncRepl()){
            SyncReplVmstoreInfo syncReplVmstoreInfoList[] = getSyncReplPolicy().getSyncReplConfig().getSyncReplVmstores();
            for(SyncReplVmstoreInfo syncReplVmstoreInfo: syncReplVmstoreInfoList){
                if(!syncReplVmstoreInfo.isCreateAsPrimary())
                    return syncReplVmstoreInfo.getHost();
            }
        }
        return "";
    }

    public SyncReplPolicy getSyncReplPolicy() {
        return syncReplPolicy;
    }

    public void setSyncReplPolicy( SyncReplPolicy syncReplPolicy ) {
        this.syncReplPolicy = syncReplPolicy;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public boolean isSyncRepl() {
        return getSyncReplPolicy()!=null && getSyncReplPolicy().getSyncReplConfig()!=null;
    }

}

class SyncReplPolicy {
    private SyncReplConfig syncReplConfig;

    public SyncReplConfig getSyncReplConfig() {
        return syncReplConfig;
    }

    public void setSyncReplConfig( SyncReplConfig syncReplConfig ) {
        this.syncReplConfig = syncReplConfig;
    }

}

class SyncReplConfig {
    private ClusterIpConfig clusterIpConfig;
    private SyncReplVmstoreInfo syncReplVmstores[];

    public SyncReplVmstoreInfo[] getSyncReplVmstores() {
        return syncReplVmstores;
    }

    public void setSyncReplVmstores(SyncReplVmstoreInfo[] syncReplVmstores) {
        this.syncReplVmstores = syncReplVmstores;
    }

    public ClusterIpConfig getClusterIpConfig() {
        return clusterIpConfig;
    }

    public void setClusterIpConfig( ClusterIpConfig clusterIpConfig ) {
        this.clusterIpConfig = clusterIpConfig;
    }
}

class ClusterIpConfig {
    private String ipAddress;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress( String ipAddress ) {
        this.ipAddress = ipAddress;
    }

}

class SyncReplVmstoreInfo {
    private String host;
    private boolean createAsPrimary;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isCreateAsPrimary() {
        return createAsPrimary;
    }

    public void setCreateAsPrimary(boolean createAsPrimary) {
        this.createAsPrimary = createAsPrimary;
    }
}
