package com.tintri.tintriapp.model;

import java.util.List;

/**
 * Created by shosharma on 12/9/17.
 */

public class VMStorePool implements TintriObject {
    private String name;
    private List<VMStore> members;

    public void setName(String name) {
        this.name = name;
    }

    public void setMembers(List<VMStore> members) {
        this.members = members;
    }

    // String getters
    @Override
    public String getName() {
        return name;
    }

    public String getMembers() {
        String members = "[";
        for(VMStore vmStore: this.members){
            members = members + vmStore.getName() + ",";
        }
        return members + "]";
    }
}

