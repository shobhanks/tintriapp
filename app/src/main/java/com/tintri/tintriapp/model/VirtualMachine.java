package com.tintri.tintriapp.model;

/**
 * Created by shosharma on 11/25/17.
 */

public class VirtualMachine implements TintriObject{
    private String name;
    private Vmware vmware;
    private UUID uuid;
    private Replication replication;
    private boolean isLive;
    private String vmstoreName;
//    private PolicyErrors policyErrors;

    public String getName() {
        return vmware.getName();
    }

    public String getID() {
        return uuid.getUuid();
    }

    public String getReplicationState(){
        return replication.getReplicationState();
    }

    public Vmware getVmware() {
        return vmware;
    }

    public void setVmware(Vmware vmware) {
        this.vmware = vmware;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Replication getReplication() {
        return replication;
    }

    public void setReplication(Replication replication) {
        this.replication = replication;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }

    public String getVmstoreName() {
        return vmstoreName;
    }

    public void setVmstoreName(String vmstoreName) {
        this.vmstoreName = vmstoreName;
    }

//    public PolicyErrors getPolicyErrors() {
//        return policyErrors;
//    }
//
//    public void setPolicyErrors(PolicyErrors policyErrors) {
//        this.policyErrors = policyErrors;
//    }

    @Override
    public String toString() {
        return "VirtualMachine{" +
                "vmware=" + vmware +
                ", uuid=" + uuid +
                ", replication=" + replication +
                ", isLive=" + isLive +
                ", vmstoreName='" + vmstoreName + '\'' +
                ", policyErrors=" +
                '}';
    }

    public class Vmware {
        private String name;
        private String hypervisorPath;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getHypervisorPath() {
            return hypervisorPath;
        }

        public void setHypervisorPath(String hypervisorPath) {
            this.hypervisorPath = hypervisorPath;
        }

        @Override
        public String toString() {
            return "Vmware{" +
                    "name='" + name + '\'' +
                    ", hypervisorPath='" + hypervisorPath + '\'' +
                    '}';
        }
    }

    public class Replication {
        private String replicationState;

        public String getReplicationState() {
            return replicationState;
        }

        public void setReplicationState(String replicationState) {
            this.replicationState = replicationState;
        }

        @Override
        public String toString() {
            return "Replication{" +
                    "replicationState='" + replicationState + '\'' +
                    '}';
        }
    }

    public class PolicyErrors {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return "PolicyErrors{" +
                    "message='" + message + '\'' +
                    '}';
        }
    }
}

