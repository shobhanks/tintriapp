package com.tintri.tintriapp.model;

/**
 * Created by shosharma on 12/10/17.
 */

class UUID {
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
