package com.tintri.tintriapp.model;

/**
 * Created by shosharma on 12/9/17.
 */

public class VMStore implements TintriObject {
    private String hostname;
    private String applianceId;
    private ApplianceInfo applianceInfo;
    private SessionStatusInfo sessionStatusInfo;

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    // String getters
    @Override
    public String getName() {
        return hostname;
    }

    public String getApplianceId() {
        return applianceId;
    }

    public String getSerialNumber() {
        return applianceInfo.getSerialNumber();
    }

    public String getModelName() {
        return applianceInfo.getModelName();
    }

    public String getOsVersion() {
        return applianceInfo.getOsVersion();
    }

    public String getIsAllFlash() {
        return String.valueOf(applianceInfo.isAllFlash());
    }

    public String getCurrentCapacityGiB() {
        return applianceInfo.getCurrentCapacityGiB();
    }

    public String getIsFipsEncryptionEnabled() {
        return String.valueOf(applianceInfo.isFipsEncryptionEnabled());
    }

    public String getConnected() {
        return String.valueOf(sessionStatusInfo.isConnected());
    }

    public void setApplianceId(String applianceId) {
        this.applianceId = applianceId;
    }

    public ApplianceInfo getApplianceInfo() {
        return applianceInfo;
    }

    public void setApplianceInfo(ApplianceInfo applianceInfo) {
        this.applianceInfo = applianceInfo;
    }

    public SessionStatusInfo getSessionStatusInfo() {
        return sessionStatusInfo;
    }

    public void setSessionStatusInfo(SessionStatusInfo sessionStatusInfo) {
        this.sessionStatusInfo = sessionStatusInfo;
    }


    public class ApplianceInfo {
        private String serialNumber;
        private String modelName;
        private String osVersion;
        private boolean isAllFlash;
        private String currentCapacityGiB;
        private boolean isFipsEncryptionEnabled;


        public String getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public String getOsVersion() {
            return osVersion;
        }

        public void setOsVersion(String osVersion) {
            this.osVersion = osVersion;
        }

        public boolean isAllFlash() {
            return isAllFlash;
        }

        public void setAllFlash(boolean allFlash) {
            isAllFlash = allFlash;
        }

        public String getCurrentCapacityGiB() {
            return currentCapacityGiB;
        }

        public void setCurrentCapacityGiB(String currentCapacityGiB) {
            this.currentCapacityGiB = currentCapacityGiB;
        }

        public boolean isFipsEncryptionEnabled() {
            return isFipsEncryptionEnabled;
        }

        public void setFipsEncryptionEnabled(boolean fipsEncryptionEnabled) {
            isFipsEncryptionEnabled = fipsEncryptionEnabled;
        }
    }

    public class SessionStatusInfo {
        private boolean connected;

        public boolean isConnected() {
            return connected;
        }

        public void setConnected(boolean connected) {
            this.connected = connected;
        }
    }
}
