package com.tintri.tintriapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.tintri.tintriapp.request.AuthenticationRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText name;
    private EditText password;
    private Button login;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = (EditText) findViewById(R.id.txtName);
        password = (EditText) findViewById(R.id.txtPassword);
        login = (Button) findViewById(R.id.btnLogin);
        result = (TextView) findViewById(R.id.txtLoginResult);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    authenticate(name.getText().toString(), password.getText().toString());
                } catch (JSONException e) {
                    Log.e(Constants.TAG, e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        TintriUtil.handleSSLHandshake();
    }

    private void authenticate(String userName, String password) throws JSONException {
        Constants.HOST_NAME = ((EditText) findViewById(R.id.txtHostName)).getText().toString();
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        AuthenticationRequest request = new AuthenticationRequest(Request.Method.POST,
                Constants.getLoginURLString(),
                getLoginJsonObject(userName, password).toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("Cookie",response);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        Log.d(Constants.TAG, "Starting display activity");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        result.setTextColor(Color.RED);
                        if(error.getClass().equals(com.android.volley.NoConnectionError.class))
                            result.setText("Unable to connect to " + Constants.HOST_NAME);
                        else
                            result.setText("Invalid Login");
                        Log.e(Constants.TAG, error+"here");
                    }
                });
        requestQueue.add(request);
    }

    private JSONObject getLoginJsonObject(String username, String password) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", username);
        jsonObject.put("password", password);
        String str[] = new String[1];
        jsonObject.put("roles", str);
        jsonObject.put("newPassword", "");
        jsonObject.put("typeId", "com.tintri.api.rest.vcommon.dto.rbac.RestApiCredentials");
        jsonObject.put("fullApiVersion", "");
        JSONArray jsonArray = new JSONArray();
        return jsonObject;
    }

}
